'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class book extends Model {
    /**s
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  book.init({
    nama: DataTypes.STRING,
    harga: DataTypes.INTEGER,
    tahun: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'book',
  });
  return book;
};
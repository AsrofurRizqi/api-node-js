const mainRoutes = require("express").Router();
const bookRoutes = require("./BookRoutes");

mainRoutes.use("/buku", bookRoutes);

module.exports= mainRoutes;
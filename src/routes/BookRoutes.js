const BookRoutes = require ("express").Router();
const bukuControllers = require("../controllers/bukuControllers")

BookRoutes.get("/", bukuControllers.getAllBuku);
BookRoutes.post("/", bukuControllers.postBuku);
BookRoutes.get("/:id", bukuControllers.getDataById);
BookRoutes.delete("/:id", bukuControllers.deleteData);
BookRoutes.put("/:id", bukuControllers.putDataById);

module.exports = BookRoutes;
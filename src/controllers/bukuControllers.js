const {book} = require("../models/");

module.exports = {
    getAllBuku : (req, res)=>{
        book.findAll()
        .then((data)=>{
            res.send({
                msg: "Success get all data",
                status: 200,
                data
            })
        })
        .catch ((err)=>{
            res.send({
                msg: 'Error to get all data',
                status:500,
                err,
            })
        })
    },
    postBuku : (req, res)=>{
        let{body} = req;
        book.create(body)
        .then((data)=>{
            res.status(200).send({
                msg: "Success to post data",
                status : 200,
                data
            })
        })
        .catch((err)=>{
            res.status(500).send({
                msg: 'Failed to post data',
                status: 500,
                err,
            })
        })
    },
    getDataById :(req,res)=>{
        let{ id }=req.params;
        book.findOne({
            where : {id},
        })
        .then ((data)=>{
            res.status(200).send({
                msg: 'Success get data By Id',
                status:200,
                data
            })
        })
        .catch((err)=>{
            res.status(500).send({
                msg: 'Failed get data By Id',
                status: 500,
                err,
            })
        })
    },
    deleteData :(req, res)=>{
        let{id}=req.params;
        book.destroy({
            where : {id},
        })
        .then((data)=>{
            res.status(200).send({
                msg: 'Success delete data',
                status: 200,
                data
            })
        })
        .catch((err)=>{
            res.status(500).send({
                msg: 'Failed delete data',
                status: 500,
                err,
            })
        })
    },
    putDataById: (req, res)=>{
        let{id}=req.params;
        let{body}=req;
        book.update(body,{
            where:{id}
        })
        .then((data)=>{
            res.status(200).send({
                msg: 'Succes put data by id',
                status: 200,
                data
            })
        })
        .catch((err)=>{
            res.status(500).send({
                msg: 'Failed put data by id',
                msg: 500,
                err,
            })
        })
    }

}
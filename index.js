require("dotenv").config({});
const express = require('express');
const app = express();
const port = process.env.PORT || 3003;
const cors = require('cors');

app.use(cors())
const mainRoutes = require('./src/routes/');
app.use(express.urlencoded({extended: false}));
app.use('/', mainRoutes)


app.listen(port ,() =>{
    console.log("server jalan pada port"+port);
})